// ignore_for_file: camel_case_types
import 'dart:convert';
import 'package:http/http.dart' show Client;

// import 'package:flutter/services.dart';
class GetPost {
  String url;
  GetPost(this.url);

  var baseUrl;
  void init() {
    baseUrl =
        Uri.https('graphql.papermindvention.com', '/api/blog', {"target": url});
  }

  Client client = Client();
  Future<Map> getData() async {
    final response = await client.get(baseUrl);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return {};
    }
  }
}
