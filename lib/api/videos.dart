// ignore_for_file: camel_case_types
import 'dart:convert';
import 'package:http/http.dart' show Client;

class GetVideos {
  GetVideos();

  var baseUrl = Uri.https('learning-2ff9a-default-rtdb.firebaseio.com',
      'movies.json', {"print": "pretty"});

  Client client = Client();
  Future<List> getData() async {
    final response = await client.get(baseUrl);
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return [];
    }
  }
}
