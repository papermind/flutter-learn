import 'package:basic/components/tabs.dart';
import 'package:basic/page/details.dart';
import 'package:basic/page/post_categories.dart';
import 'package:basic/page/video.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Nav2App());
}

class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      debugShowCheckedModeBanner: false,
      title: "Learning Basic",
      routes: {
        '/': (context) => Tabs(),
        '/details': (context) => DetailScreen(),
        '/video': (context) => VideoShow(),
        '/post-categories': (context) => PostCategory(),
      },
    );
  }
}
