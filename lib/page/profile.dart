import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

class ProfileScreen extends StatelessWidget {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController bioController = TextEditingController();

  changeProfilePhoto(BuildContext parentContext) {
    return showDialog(
      context: parentContext,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Change Photo'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Changing your profile photo has not been implemented yet'),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildTextField({String name, TextEditingController controller}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 12.0),
          child: Text(
            name,
            style: TextStyle(color: Colors.grey),
          ),
        ),
        CupertinoTextField(placeholder: name)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
          child: CircleAvatar(
            backgroundImage: NetworkImage(
                "http://s7.sharemydrive.xyz/wp-content/uploads/2021/07/film-trollhunters-rise-of-the-titans-2021-lk21.jpg"),
            radius: 250.0,
          ),
        ),
        ElevatedButton(
            onPressed: () {
              changeProfilePhoto(context);
            },
            child: Text(
              "Change Photo",
              style: const TextStyle(
                  color: Colors.blue,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            )),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              buildTextField(name: "Name", controller: nameController),
              buildTextField(name: "Bio", controller: bioController),
            ],
          ),
        ),
        Padding(
            padding: const EdgeInsets.all(16.0),
            child: MaterialButton(
                onPressed: () => {_logout(context)}, child: Text("Logout")))
      ],
    );
  }

  void _logout(BuildContext context) async {
    print("logout");
  }
}
