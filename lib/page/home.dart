import 'package:basic/api/posts.dart';
import 'package:basic/components/carousel.dart';
import 'package:basic/models/post.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  GetPost getPost;
  List _items = [];
  int page = 1;
  Future<void> readJson() async {
    getPost = GetPost(
        "https://id.techinasia.com/wp-json/techinasia/2.0/categories/premium-content/posts?page=$page&per_page=4");
    getPost.init();
    final result = await getPost.getData();
    if (result['posts'].length > 0) {
      setState(() {
        _items = result['posts'];
      });
    }
  }

  Future<void> loadMore() async {
    getPost = GetPost(
        "https://id.techinasia.com/wp-json/techinasia/3.0/posts?page=$page&per_page=10");
    getPost.init();
    final result = await getPost.getData();
    if (result['posts'].length > 0) {
      setState(() {
        _items = [..._items, ...result['posts']];
      });
    }
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  Widget build(BuildContext context) {
    if (_items.length == 0) {
      // return Center(child: LinearProgressIndicator());
      return ListView(children: [
        ...List.generate(
            20,
            (index) => SizedBox(
                  width: 350,
                  height: 20,
                  child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade900,
                      highlightColor: Colors.grey.shade200,
                      child: ListTile(
                        leading: Icon(CupertinoIcons.archivebox),
                        title: Text('######################'),
                        subtitle: Text('------------------'),
                      )),
                ))
      ]);
    } else {
      page++;
      return _buildList(_items);
    }
  }

  Widget _buildList(List data) {
    _items.forEach((element) {
      CachedNetworkImage(
        imageUrl: element['featured_image']['source'],
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    });
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: Text(
            'Learning',
          ),
        ),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: ListView(children: <Widget>[
              Carousel(imgList: data.sublist(0, 4)),
              ...data.map(
                (e) => Card(
                    margin: EdgeInsets.all(1),
                    child: ListTile(
                      leading: Image(
                        image: CachedNetworkImageProvider(
                            e['featured_image']['source']),
                        fit: BoxFit.cover,
                        height: 200,
                      ),
                      title: Html(data: e["title"]),
                      subtitle: Html(
                          data: e["excerpt"],
                          style: {"p": Style(fontSize: FontSize(10))}),
                      onTap: () => Navigator.pushNamed(context, '/details',
                          arguments: Post(
                              id: e['id'],
                              title: e['title'],
                              excerpt: e['excerpt'],
                              content: e['content'],
                              featured_image: e['featured_image'],
                              modified: e['modified'],
                              author: e['author'],
                              slug: e['slug'],
                              categories: e['categories'])),
                    )),
              ),
              CupertinoButton(
                child: Text('Load More'),
                onPressed: () => loadMore(),
              ),
            ])));
  }
}
