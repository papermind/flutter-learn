import 'package:basic/models/video.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:flutter_html/shims/dart_ui_real.dart';

class Videos extends StatefulWidget {
  @override
  _VideosScreen createState() => _VideosScreen();
}

class _VideosScreen extends State<Videos> {
  List _items = [];
  // Fetch content from the json file
  Future<void> readJson() async {
    final String response = await rootBundle.loadString('static/movies.json');
    final data = await json.decode(response);
    setState(() {
      _items = data;
    });
  }

  @override
  void initState() {
    super.initState();
    readJson();
  }

  Widget build(BuildContext context) {
    readJson();
    _items.forEach((element) {
      CachedNetworkImage(
        imageUrl: element['image'],
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    });
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: Text(
            'Movies',
          ),
        ),
        child: GridView.count(
          crossAxisCount: 2,
          primary: false,
          padding: const EdgeInsets.all(1),
          children: <Widget>[
            ..._items.map((e) => CupertinoButton(
                padding: EdgeInsets.all(1),
                child: Image(
                  image: CachedNetworkImageProvider(e['image']),
                  fit: BoxFit.cover,
                  width: window.physicalSize.width / 2,
                  height: window.physicalSize.height / 2.5,
                ),
                onPressed: () => Navigator.pushNamed(context, "/video",
                    arguments: Video.fromJson(e)))),
          ],
        ));
  }
}
