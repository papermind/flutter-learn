import 'package:basic/models/video.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class VideoShow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as Video;
    final Map data = args.toJson();
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(data['title']),
      ),
      child: Center(
          child: ListView(children: <Widget>[
        Image.network(data['image'], fit: BoxFit.fitWidth, scale: 2),
        Text(data['title']),
        Card(
            margin: EdgeInsets.all(1),
            child: ListTile(
              leading: Text(data['rating']),
              title: Text(data['category']),
              subtitle: Text(data['source']),
            )),
        Card(
            margin: EdgeInsets.all(1),
            child: ListTile(
              leading: Text(data['quality']),
              title: Text(data['created_at']),
              subtitle: Text(data['movie']),
            )),
        Html(data: data['description']),
      ])),
    );
  }
}
