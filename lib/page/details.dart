import 'package:basic/models/post.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as Post;
    final data = args.toJson();
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(data['title']),
      ),
      child: Center(
          child: ListView(children: <Widget>[
        Image.network(data['featured_image']),
        Text(data['modified']),
        Card(
            margin: EdgeInsets.all(1),
            child: ListTile(
              leading: Image.network(data['avatar']),
              title: Text(data['name']),
              subtitle: Text(data['description']),
            )),
        Html(data: data['content']),
        Text(data['categories']),
      ])),
    );
  }
}
