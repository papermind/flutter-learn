import 'package:basic/api/posts.dart';
import 'package:basic/models/category.dart';
import 'package:basic/models/post.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:shimmer/shimmer.dart';

class PostCategory extends StatefulWidget {
  @override
  _PostCategory createState() => _PostCategory();
}

class _PostCategory extends State<PostCategory> {
  GetPost getPost;
  List _items = [];
  int page = 1;
  String target = "";
  Future<void> readJson(String link) async {
    target = link;
    getPost = GetPost("$link?page=$page&per_page=8");
    getPost.init();
    final result = await getPost.getData();
    if (result['posts'].length > 0) {
      setState(() {
        _items = [..._items, ...result['posts']];
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as Category;
    if (_items.length == 0 && page == 1) {
      readJson(args.link);
      // return Center(child: LinearProgressIndicator());
      return ListView(children: [
        ...List.generate(
            20,
            (index) => SizedBox(
                  width: 350,
                  height: 20,
                  child: Shimmer.fromColors(
                      baseColor: Colors.grey.shade900,
                      highlightColor: Colors.grey.shade200,
                      child: ListTile(
                        leading: Icon(CupertinoIcons.archivebox),
                        title: Text('######################'),
                        subtitle: Text('------------------'),
                      )),
                ))
      ]);
    } else {
      page++;
      return _buildList(_items, args);
    }
  }

  Widget _buildList(List data, Category args) {
    _items.forEach((element) {
      CachedNetworkImage(
        imageUrl: element['featured_image']['source'],
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(value: downloadProgress.progress),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    });
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          middle: Text(
            'Learning',
          ),
        ),
        child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: ListView(children: <Widget>[
              ...data.map(
                (e) => Card(
                    margin: EdgeInsets.all(1),
                    child: ListTile(
                      leading: Image(
                        image: CachedNetworkImageProvider(
                            e['featured_image']['source']),
                        fit: BoxFit.fill,
                        height: 200,
                      ),
                      title: Html(data: e["title"]),
                      subtitle: Html(
                          data: e["excerpt"],
                          style: {"p": Style(fontSize: FontSize(10))}),
                      onTap: () => Navigator.pushNamed(context, '/details',
                          arguments: Post(
                              id: e['id'],
                              title: e['title'],
                              excerpt: e['excerpt'],
                              content: e['content'],
                              featured_image: e['featured_image'],
                              modified: e['modified'],
                              author: e['author'],
                              slug: e['slug'],
                              categories: e['categories'])),
                    )),
              ),
              CupertinoButton(
                child: Text('Load More'),
                onPressed: () => readJson(args.link),
              ),
            ])));
  }
}
