import 'dart:convert';

class Post {
  String id;
  String title;
  String excerpt;
  String content;
  Map featured_image;
  String modified;
  Map author;
  String slug;
  List categories;
  String avatar;

  Post(
      {this.id,
      this.title,
      this.excerpt,
      this.content,
      this.featured_image,
      this.modified,
      this.author,
      this.slug,
      this.categories});

  factory Post.fromJson(Map<String, dynamic> map) {
    return Post(
        id: map["id"],
        title: map["title"],
        excerpt: map["excerpt"],
        content: map["content"],
        featured_image: map["featured_image"],
        modified: map["modified"],
        author: map['author'],
        slug: map['slug'],
        categories: map['categories']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "title": title,
      "excerpt": excerpt,
      "content": content,
      "featured_image": featured_image['source'],
      "modified": modified,
      // "author": author,
      "name": author['display_name'],
      "avatar": author['avatar_url'],
      "description": author['description'],
      "slug": slug,
      "categories": categories.map((e) => e['name']).toString(),
    };
  }

  @override
  String toString() {
    return 'Post{id: $id, title: $title, excerpt: $excerpt, content: $content, featured_image:$featured_image, modified:$modified}';
  }
}

List<Post> PostFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Post>.from(data.map((item) => Post.fromJson(item)));
}

String PostToJson(Post data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
