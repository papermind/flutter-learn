import 'dart:convert';

class Video {
  int id;
  String category;
  String created_at;
  String description;
  String image;
  String movie;
  String quality;
  String rating;
  String source;
  String title;
  String updated_at;

  Video(
      {this.category,
      this.created_at,
      this.description,
      this.id,
      this.image,
      this.movie,
      this.quality,
      this.rating,
      this.source,
      this.title,
      this.updated_at});

  factory Video.fromJson(Map<String, dynamic> map) {
    return Video(
        category: map["category"],
        created_at: map["created_at"],
        description: map["description"],
        id: map["id"],
        image: map["image"],
        movie: map["movie"],
        quality: map["quality"],
        rating: map["rating"],
        source: map["source"],
        title: map["title"],
        updated_at: map["updated_at"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "category": category,
      "created_at": created_at,
      "description": description,
      "id": id,
      "image": image,
      "movie": movie,
      "quality": quality,
      "rating": rating,
      "source": source,
      "title": title,
      "updated_at": updated_at
    };
  }

  @override
  String toString() {
    return 'Video{category:$category,created_at:$created_at,description:$description,id:$id,image:$image,movie:$movie,quality:$quality,rating:$rating,source:$source,title:$title,updated_at:$updated_at}';
  }
}

List<Video> PostFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Video>.from(data.map((item) => Video.fromJson(item)));
}

String PostToJson(Video data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}
