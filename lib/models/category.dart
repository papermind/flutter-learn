class Category {
  Category({
    this.text,
    this.link,
  });

  factory Category.fromJson(Map<String, dynamic> map) {
    return Category(
      text: map['text'],
      link: map['link'],
    );
  }
  Map<String, dynamic> toJson() {
    return {"text": text, "link": link};
  }

  final String text;
  final String link;
}
