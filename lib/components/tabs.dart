import 'package:basic/page/categories.dart';
import 'package:basic/page/home.dart';
import 'package:basic/page/profile.dart';
import 'package:basic/page/videos.dart';
import 'package:flutter/cupertino.dart';

class Tabs extends StatelessWidget {
  // const Tabs({Key?key,"title"}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(items: [
          BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.home), label: "Home"),
          BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.grid_circle), label: "Categories"),
          BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.videocam_circle_fill), label: "Videos"),
          BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.person), label: "User Info")
        ]),
        tabBuilder: (context, index) {
          switch (index) {
            case 0:
              return HomeScreen();
              break;
            case 1:
              return Categories();
              break;
            case 2:
              return Videos();
              break;
            case 3:
              return ProfileScreen();
              break;
            default:
              return HomeScreen();
              break;
          }
        });
  }
}
